infoarticulos =[
  {'articulo':'Juguete','precio':88800.00, 'precioFinal':0.00},
  {'articulo':'Celular','precio':179900.00, 'precioFinal':0.00},
  {'articulo':'Horno','precio':699900.00, 'precioFinal':0.00},
  {'articulo':'Escritorio','precio':594900.00, 'precioFinal':0.00},
  {'articulo':'Bicicleta','precio':729900.00, 'precioFinal':0.00}]

precioTotal = 0.00
iva = 1.19

def obtenerformatoMoneda(precio):
      return "{:0,.2f}".format(float(precio))
    

print("Calcular el precio  final de cada producto y el precio de la compra")

for articulo in infoarticulos:
      tieneImpuesto = input(f"¿Aplicar el IVA al producto {articulo['articulo']} ($ {obtenerformatoMoneda(articulo['precio'])})? Y/n: ")
      if tieneImpuesto.lower() == "y":
            articulo.update({'precioFinal': articulo['precio']*iva})
      elif tieneImpuesto.lower() == "n":
            articulo.update({'precioFinal': articulo['precio']})
      print(f"El precio final del articulo {articulo['articulo']} es : ${obtenerformatoMoneda(articulo['precioFinal'])}")
      precioTotal = precioTotal +articulo['precioFinal']

print(f"El precio total de la compra es: ${obtenerformatoMoneda(precioTotal)}")
